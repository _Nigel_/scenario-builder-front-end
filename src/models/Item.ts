export default interface Item {
    id: number;
    name: String;
    description: String;
}