import Item from '../models/Item'
import axios from 'axios';

const rootApi = process.env.VUE_APP_ROOT_API;

export default class ItemService {

    public async getItems(): Promise<Item[]> {
        const response = await axios.get(process.env.VUE_APP_ROOT_API + "items");
        return response.data as Item[];
    }

    public async getItem(id: number): Promise<Item> {
        const response = await axios.get(rootApi + "items" + id);
        return response.data as Item;
    }

    public async addItem(obj: Item): Promise<Item> {
        const response = await axios.post(rootApi + "items", obj);
        return response.data as Item;
    }

    public async deleteItem(id: number): Promise<string> {
        const response = await axios.delete(rootApi + "items/" + id);
        return response.status.toString();
    }
}