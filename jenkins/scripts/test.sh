#!/usr/bin/env sh
set -x
npm install --save-dev
set +x
set -x
npm --version
node --version
npm run test:unit